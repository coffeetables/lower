{
  self,
  modulesPath,
  lib,
  config,
  pkgs,
  ...
}: {
  imports =
    [
      # Include the results of the hardware scan.
      "${modulesPath}/installer/scan/not-detected.nix"
    ]
    ++ self.nixosSuites.below;

  home-manager.users.pachums = {
    imports = self.homeSuites.below;
  };

  services.fwupd.enable = true;

  networking.hostName = "belowTheCoffeeTable"; # Define your hostname.

  # boot.binfmt.emulatedSystems = ["aarch64-linux"];

  services.fprintd.enable = true;

  ## BootLoader configuration
  boot.loader.grub = {
    enable = true;
    configurationLimit = 20;
    efiInstallAsRemovable = true;
    devices = ["nodev"];
    efiSupport = true;
    fsIdentifier = "uuid";
  };

  system.stateVersion = "24.05";

  boot.initrd.availableKernelModules = [ "xhci_pci" "thunderbolt" "nvme" "usb_storage" "sd_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];
  boot.kernelParams = [ "i915.force_probe=46a6" ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/4a8bdbcd-bf5e-468c-a716-3ab55448fb5d";
      fsType = "ext4";
    };

  boot.initrd.luks.devices."luks-54ee75ac-5494-40d6-bf06-b6a2a086b7dd".device = "/dev/disk/by-uuid/54ee75ac-5494-40d6-bf06-b6a2a086b7dd";

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/C8B6-5304";
      fsType = "vfat";
      options = [ "fmask=0022" "dmask=0022" ];
    };

  swapDevices = [
    {
      device = "/swapfile";
      size = 12 * 1024; # 12GB
    }
  ];



  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.wlp166s0.useDHCP = lib.mkDefault true;

  # powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
