{
  config,
  lib,
  pkgs,
  ...
}: {
  environment.systemPackages = [pkgs.droidcam];
  boot.extraModulePackages = let
    v4l2loopback = config.boot.kernelPackages.v4l2loopback.overrideAttrs (cattrs: {
      version = "0.12.7-unstable-2024-02-12-6.8.9";
      src = pkgs.fetchFromGitHub {
        owner = "umlaeute";
        repo = "v4l2loopback";
        rev = "5d72c17f92ee0e38efbb7eb85e34443ecbf1a80c";
        hash = "sha256-ggmYH5MUXhMPvA8UZ2EAG+eGoPTNbw7B8UxmmgP6CsE=";
      };
      patches = [];
    });
  in [v4l2loopback];
  boot.kernelModules = ["v4l2loopback" "snd-aloop"];
}
