{
  lib,
  pkgs,
  ...
}: {
  programs.niri = {
    enable = true;
    package = pkgs.niri;
  };

  environment.systemPackages = with pkgs; [
    gamescope
  ];

  systemd.user.services.niri-flake-polkit = {
    wants = lib.mkForce [];
    requisite = [ "graphical-session.target" ];
    partOf = [ "graphical-session.target" ];
  };

  # Ensure graphical-session.target stops when niri does
  # to allow for logout then login
  # systemd.user.services.niri = {
  #   overrideStrategy = "asDropin";
  #   requiredBy = [ "graphical-session.target" ];
  # };

  services.gnome.gnome-keyring.enable = lib.mkForce false;

  programs.ydotool.enable = true;

  # systemd.packages = [ pkgs.swayosd ];
  # environment.systemPackages = [
  #   pkgs.swayosd
  # ];
  # services.udev.packages = [ pkgs.swayosd ];
}
