{pkgs, ...}: {
  programs.phosh.enable = true;
  programs.feedbackd.enable = true;

  powerManagement.enable = true;
  services.upower.enable = true;
  # services.upower.package = pkgs.librem.upower; # Required for Torch to work

  system.replaceRuntimeDependencies = [
    {
      original = pkgs.gtk3;
      replacement = pkgs.librem.gtk3;
    }
  ];

  # Required for a lot of things
  environment.etc."machine-info".text = ''
    CHASSIS="handset"
  '';
}
