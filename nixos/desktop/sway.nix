{
  lib,
  pkgs,
  ...
}: {
  # services.xserver.displayManager.sessionPackages = [pkgs.wayfire];
  # environment.systemPackages = [
  #   pkgs.wayfire
  #   pkgs.wcm
  # ];
  programs.sway = {
    enable = true;
  };

  programs.waybar = {
    enable = true;
  };
}
