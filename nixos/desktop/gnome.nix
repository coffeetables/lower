{
  pkgs,
  inputs',
  ...
}:
{
  services.xserver = {
    desktopManager.gnome.enable = true;

    layout = "us";
    enable = true;
  };

  programs.evolution = {
    enable = true;
  };

  services.dbus.packages = [pkgs.dconf];

  environment.systemPackages = with pkgs; [
    libsecret
  ];
}
