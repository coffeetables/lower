{
  self,
  modulesPath,
  lib,
  config,
  pkgs,
  inputs,
  ...
}: {
  imports = self.nixosSuites.base;

  networking.hostName = "wsl";

  wsl.enable = true;
  wsl.defaultUser = "pachums";
  system.stateVersion = "23.11";

  users.users.pachums = {
    initialPassword = "password";
    hashedPasswordFile = lib.mkForce null;
  };

  home-manager.users.pachums = {
    imports = self.homeSuites.shell;
  };
}
