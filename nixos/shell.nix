{
  lib,
  pkgs,
  ...
}: let
  my-python-packages = python-packages:
    with python-packages; [
      jedi
      pylint
      pynvim
      pip
      tkinter
      python-lsp-server
      # other python packages you want
    ];
  python-with-my-packages = pkgs.python3.withPackages my-python-packages;
in {
  virtualisation.docker.enable = true;
  virtualisation.podman = {
    enable = false;
    dockerCompat = true;
  };

  environment.systemPackages = with pkgs; [
    distrobox
    python-with-my-packages
    nodejs
    yarn
    gparted
    vim
  ];

  services.openssh.enable = true;

  programs.extra-container.enable = true;

  nix.distributedBuilds = true;
  nix.settings.netrc-file = "/etc/nix/netrc";

  programs.ssh.extraConfig = ''
    Host builder
      Port 4422
      IdentitiesOnly yes
      User pachums
      HostName myrdd.info
      IdentityFile /home/pachums/.ssh/id_rsa
  '';

  environment.variables = {EDITOR = "vim";};

  ## Program setup
  programs = {
    java.enable = true;
    zsh.enable = true;
    adb.enable = lib.mkIf (pkgs.system == "x86_64-linux") true;
  };
}
