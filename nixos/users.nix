{
  self,
  config,
  pkgs,
  ...
}: {
  home-manager.users.pachums.home = {
    inherit (config.system) stateVersion;
  };

  users.mutableUsers = false;
  users.users.root.hashedPassword = "*";

  age.secrets.pachums.file = "${self}/secrets/pachums.age";
  age.secrets.keepass.file = "${self}/secrets/keepass.age";

  users.users.pachums = {
    hashedPasswordFile = "/run/agenix/pachums";
    isNormalUser = true;
    extraGroups = ["docker" "ydotool" "video" "vboxusers" "scanner" "wheel" "networkmanager" "input"];
    shell = pkgs.zsh;
    home = "/home/pachums";
  };
}
