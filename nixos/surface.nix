{
  self,
  modulesPath,
  lib,
  config,
  pkgs,
  ...
}: {
  imports =
    [
      # Include the results of the hardware scan.
      "${modulesPath}/installer/scan/not-detected.nix"
    ]
    ++ self.nixosSuites.below;

  home-manager.users.pachums = {
    imports = self.homeSuites.below;
  };

  # wifi config
  networking.hostName = "belowTheCoffeeTable"; # Define your hostname.

  boot.binfmt.emulatedSystems = ["aarch64-linux"];

  microsoft-surface.ipts.enable = lib.mkForce false;
  services.iptsd.enable = true;
  services.iptsd.config = {
    Touch = {
      DisableOnPalm = true;
      DisableOnStylus = true;
    };
    Contacts = {
      SizeMin = 0.803;
      SizeMax = 1.356;
      AspectMin = 1.007;
      AspectMax = 1.388;
    };
    DFT = {
      ButtonMinMag = 1000;
    };
  };

  ## BootLoader configuration
  boot.loader.grub = {
    enable = true;
    configurationLimit = 4;
    efiInstallAsRemovable = true;
    devices = ["nodev"];
    efiSupport = true;
    fsIdentifier = "uuid";
    splashImage = pkgs.fetchurl {
      url = "https://www.dementiajourney.org/wp-content/uploads/2017/10/desktop-wallpaper-books-wallpapers-bookshelf-old-ladder-book-library-vintage-cartoon.jpg";
      sha256 = "0ip2ps5ilciib525qvsmkmd2pcg9zcwkfmgnpw972nwh2zf1ypad";
    };
  };

  boot.initrd.availableKernelModules = ["xhci_pci" "nvme" "usb_storage" "sd_mod"];
  boot.initrd.kernelModules = [];
  boot.kernelModules = ["kvm-intel"];
  boot.extraModulePackages = [];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/e6b40772-8ddd-4aa6-8eea-b751af49557f";
    fsType = "ext4";
  };

  boot.initrd.luks.devices."luks-15cd7bd5-6aa0-4643-9d5a-ae8c934956e9".device = "/dev/disk/by-uuid/15cd7bd5-6aa0-4643-9d5a-ae8c934956e9";

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/0D48-4879";
    fsType = "vfat";
    options = ["fmask=0022" "dmask=0022"];
  };

  swapDevices = [
    {
      device = "/swapfile";
      size = 12 * 1024; # 16GB
    }
  ];

  system.stateVersion = "23.11";
  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.wlp166s0.useDHCP = lib.mkDefault true;

  # powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
