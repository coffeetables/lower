{
  config,
  lib,
  pkgs,
  ...
}: let
  allPackages =
    config.environment.systemPackages
    ++ (config.home-manager.users.pachums.home.packages or []);
  hasKdeconnect =
    (lib.elem pkgs.gnomeExtensions.gsconnect allPackages)
    || (config.home-manager.users.pachums.services.kdeconnect.enable or false);
in {
  services.mullvad-vpn.enable = true;
  services.mullvad-vpn.package = pkgs.mullvad-vpn;

  networking.networkmanager.enable = !config.networking.wireless.enable;

  networking.firewall.trustedInterfaces = [ "p2p-wl+" ];
  # For miracast
  networking.firewall.allowedTCPPorts = [7236 7250];
  networking.firewall.allowedUDPPorts = [7236 5353];

  networking.firewall.allowedTCPPortRanges = lib.mkIf hasKdeconnect [
   {
     from = 1714;
     to = 1764;
   }
  ];
  networking.firewall.allowedUDPPortRanges = lib.mkIf hasKdeconnect [
    {
      from = 1714;
      to = 1764;
    }
  ];

  # services.tailscale = {
  #   enable = true;
  # };
}
