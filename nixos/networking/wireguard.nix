{
  self,
  lib,
  ...
}: {
  age.secrets.wireguard.file = "${self}/secrets/wireguard.age";

  systemd.services.wg-quick-wg0.wantedBy = lib.mkForce [];
  # Enable WireGuard
  networking.wg-quick.interfaces = {
    # "wg0" is the network interface name. You can name the interface arbitrarily.
    wg0 = {
      autostart = false;
      # PUBLIC KEY: "6D2bAD2kpv0GIr2qVUMeIZWQs22OOje+O4AMk+VI6j4=";
      privateKeyFile = "/run/agenix/wireguard";

      dns = [
        "1.1.1.1"
        "2606:4700:4700::1111"
      ];

      peers = [
        # For a client configuration, one peer entry for the server will suffice.

        {
          endpoint = "myrdd.info:58735";

          publicKey = "fX36hqXQNFF0QCu+ip9ih1wX1npPmtD1RfxioNPu8Uw=";

          # Forward all the traffic via VPN.
          allowedIPs = ["0.0.0.0/0" "::/0"];
          # Or forward only particular subnets
          #allowedIPs = [ "10.100.0.1/24" ];
          #allowedIPs = [ "10.100.0.1" "91.108.12.0/22" ];

          # Send keepalives every 25 seconds. Important to keep NAT tables alive.
          persistentKeepalive = 25;
        }
      ];
    };
  };
}
