let
  pachums = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJuquUD14FMiY07q1V59kg+ODN6jlxSvNBFIK3+GnYXr";
  below = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPpSlLZC876J5QQcH5fPPHl3IuOZMBgmqcUlTilRfr1r";
  allKeys = [pachums below];
in {
  "pachums.age".publicKeys = allKeys;
  "keepass.age".publicKeys = allKeys;
}
