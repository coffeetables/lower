{
  config,
  pkgs,
  ...
}: {
  home.packages = with pkgs;
    [
      samba
      # zeroad
      prismlauncher
    ]
    ++ lib.optionals (pkgs.system != "aarch64-linux") [
      lutris
      (pkgs.steam.override {extraLibraries = pkgs: [pkgs.pipewire];})
    ];
}
