{
  lib,
  config,
  pkgs,
  ...
}: {

  home.packages = with pkgs;
    [
      pavucontrol
      hugo
      go # for hugo
      giara
      # gnome-podcasts
      gimp
      vlc
      cheese
      eog
      peek
      krita
      inkscape
      pkgs.spotube
    ]
    ++ lib.optionals (pkgs.system != "aarch64-linux") [
      stremio
      spotify
      google-chrome
      newsflash # not built by hydra
    ];
}
