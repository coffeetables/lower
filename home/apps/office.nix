{
  inputs',
  lib,
  config,
  pkgs,
  ...
}: {
  programs.vscode = {
    enable = true;
    package = pkgs.vscodium;
    extensions = with pkgs.vscode-extensions; [
      vscodevim.vim
      ms-toolsai.jupyter
      ms-python.python
      mkhl.direnv
    ];
  };

  programs.alacritty.enable = true;

  home.packages = with pkgs;
    [
      meteo
      write_stylus
      pkgs.rnote
      pkgs.mmex
      jetbrains.pycharm-community
      texlive.combined.scheme-full
      seafile-client
      gnome-notes
      endeavour
      rclone-browser
      libreoffice
      simple-scan
      calibre
      hunspell
      hunspellDicts.en-us-large
    ]
    ++ lib.optionals (pkgs.system != "aarch64-linux") [
      wineWowPackages.full
    ];

  services.nextcloud-client = {
    enable = true;
    startInBackground = true;
  };

  systemd.user.services.nextcloud-client = {
    Unit = {
      After = [
        "keyring.target"
        "tray.target"
        "graphical-session.target"
      ];
    };
  };

}
