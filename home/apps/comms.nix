{
  lib,
  config,
  pkgs,
  inputs',
  ...
}: {
  lib.myPkgs.mailspring = pkgs.mailspring.overrideAttrs (old: rec {
    nativeBuildInputs = with pkgs; [
      autoPatchelfHook
      dpkg
      (wrapGAppsHook3.override {inherit (pkgs) makeWrapper;})
    ];
    # Add wayland support for electron when NIXOS_OZONE_WL is set
    # and fix issue with accessing keyring on latest electron
    # see https://github.com/NixOS/nixpkgs/pull/267823
    preFixup = ''
      gappsWrapperArgs+=(
        --add-flags "\''${NIXOS_OZONE_WL:+\''${WAYLAND_DISPLAY:+--ozone-platform-hint=auto --enable-features=WaylandWindowDecorations}}"
        --add-flags '--password-store="gnome-libsecret"'
      )
    '';
  });
  home.packages = with pkgs;
    [
      # chatty
      element-desktop
      # fractal
    ]
    ++ lib.optionals (pkgs.system != "aarch64-linux") [
      ripcord
      slack
      discord
      zoom-us
      pkgs.beeper
      config.lib.myPkgs.mailspring
    ];

  programs.thunderbird = {
    enable = true;
    profiles.main = {
      isDefault = true;
      userChrome = ''
        /* Hide tab bar in Thunderbird */
        #tabs-toolbar {
          visibility: collapse !important;
        }
      '';
    };
  };

  systemd.user.services.thunderbird = {
    Unit = {
      Description = "Thunderbird Email Client";
      After = [
        "graphical-session.target"
      ];
      PartOf = [ "graphical-session.target" ];
    };

    Service = {
      Environment = "PATH=${config.home.profileDirectory}/bin";
      ExecStart = "${pkgs.thunderbird}/bin/thunderbird";
      Restart = "on-failure";
    };

    Install = { WantedBy = [ "graphical-session.target" ]; };
  };

  systemd.user.services.beeper = lib.mkIf false {
    Unit = {
      Description = "Beeper Messaging Client";
      After = [
        "keyring.target"
        "tray.target"
        "graphical-session.target"
      ];
      Wants = [
        "keyring.target"
        "tray.target"
      ];
      PartOf = [ "graphical-session.target" ];
    };

    Service = {
      Environment = "PATH=${config.home.profileDirectory}/bin";
      ExecStart = "${pkgs.beeper}/bin/beeper";
    };

    Install = { WantedBy = [ "graphical-session.target" ]; };
  };

  services.pantalaimon = {
    enable = true;
    # fix startup error
    # package = pkgs.pantalaimon.overrideAttrs (old: {
    #   nativeBuildInputs = [
    #     pkgs.wrapGAppsHook3
    #   ];

    #   dontWrapGApps = true;
    #   makeWrapperArgs = [
    #     "\${gappsWrapperArgs[@]}"
    #   ];
    # });
    settings = {
      Default = {
        LogLevel = "Debug";
        SSL = true;
      };
      Beeper = {
        Homeserver = "https://matrix.beeper.com";
        ListenAddress = "127.0.0.1";
        ListenPort = 8009;
        SSL = false;
        UseKeyring = false;
      };
    };
  };

    systemd.user.services.mailspring = lib.mkIf false {
    Unit = {
      Description = "Mailspring Email Client";
      After = [
        "keyring.target"
        "tray.target"
        "graphical-session.target"
      ];
      PartOf = [ "graphical-session.target" ];
    };

    Service = {
      Environment = "PATH=${config.home.profileDirectory}/bin";
      ExecStart = "${config.lib.myPkgs.mailspring}/bin/mailspring";
      Restart = "on-failure";
    };

    Install = { WantedBy = [ "graphical-session.target" ]; };
  };
}
