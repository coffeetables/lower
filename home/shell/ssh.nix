{
  config,
  pkgs,
  ...
}: {
  programs.ssh = {
    enable = true;
    serverAliveInterval = 20;
    matchBlocks = {
      myrdd = {
        hostname = "myrdd.info";
      };
      anju = {
        hostname = "anjutech.com";
        port = 9812;
      };
      phone = {
        user = "alarm";
        hostname = "192.168.86.200";
        port = 22;
      };
      fo_qa = {
        hostname = "5.78.68.209";
        user = "root";
        identityFile = "~/.ssh/fo_hetzner.pem";
      };
    };
  };

  home.packages = [pkgs.eternal-terminal];
}
