{
  config,
  pkgs,
  ...
}: {
  home.sessionVariables.SUDO_ASKPASS = "${pkgs.seahorse}/libexec/ssh-askpass";

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
    enableZshIntegration = true;
  };

  home.packages = with pkgs;
    [
      atop
      wget
      unzip
      git
      atool
      unrar
      gnupg
      pstree
      nix-prefetch-scripts
      nixpkgs-review
      gradle
      unrar
      mercurial
      gnumake
      tmux
      zip
      nmap
      rclone
      imagemagick
      gitAndTools.hub
      usbutils
      nixpkgs-review
      ddgr
      xclip
    ]
    ++ lib.optionals (pkgs.system != "aarch64-linux") [
      universal-ctags
      ctagsWrapped.ctagsWrapped
    ];
}
