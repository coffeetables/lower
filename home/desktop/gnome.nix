{
  config,
  pkgs,
  inputs',
  inputs,
  ...
}: {
  home.packages = with pkgs;
    [
      gnome.gnome-tweaks
      gnome.dconf-editor
      dconf2nix
    ]
    ++ (with gnomeExtensions; [
      gnome.gpaste
      # vertical-overview
      gsconnect
      appindicator
    ]);

  services.gnome-keyring = {
    enable = true;
    components = ["pkcs11" "secrets" "ssh"];
  };
}
