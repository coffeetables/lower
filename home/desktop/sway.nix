{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.wayland.windowManager.sway;
in {
  wayland.windowManager.sway = {
    enable = true;
    config = {
      terminal = "${pkgs.alacritty}/bin/alacritty";
      # modifier = "Mod4";
      # keybindings = {
      #   "${cfg.config.modifier}+p" = "${pkgs.bemenu}/bin/bemenu";
      # };
    };
  };
  home.packages = with pkgs; [
    pavucontrol
    pw-volume
  ];
}
