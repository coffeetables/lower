# Generated via dconf2nix: https://github.com/gvolpe/dconf2nix
{lib, ...}: let
  mkTuple = lib.hm.gvariant.mkTuple;
in {
  dconf.settings = {
    "org/gnome/mutter" = {
      attach-modal-dialogs = true;
      dynamic-workspaces = true;
      edge-tiling = true;
      focus-change-on-pointer-rest = true;
      workspaces-only-on-primary = true;
      experimental-features = ["scale-monitor-framebuffer"];
    };

    "org/gnome/Geary" = {
      ask-open-attachment = true;
      autoselect = false;
      compose-as-html = true;
      composer-window-size = [934 986];
      display-preview = true;
      folder-list-pane-horizontal = true;
      folder-list-pane-position-horizontal = 164;
      formatting-toolbar-visible = true;
      messages-pane-position = 572;
      migrated-config = true;
      startup-notifications = false;
    };

    "org/gnome/desktop/interface" = {
      clock-show-date = true;
      clock-show-seconds = false;
      clock-show-weekday = true;
      enable-animations = true;
      enable-hot-corners = true;
      font-antialiasing = "grayscale";
      font-hinting = "slight";
      gtk-im-module = "ibus";
      locate-pointer = false;
      show-battery-percentage = true;
      toolkit-accessibility = false;
    };

    "org/gnome/desktop/notifications" = {
      application-children = ["org-gnome-evolution" "gnome-shell-extension-prefs" "org-gnome-software" "gnome-network-panel" "firefox" "gnome-power-panel" "org-gnome-evolution-alarm-notify" "org-gnome-extensions-desktop" "org-gnome-shell-extensions" "thunderbird" "gnome-control-center" "org-gnome-geary" "element-desktop" "org-gnome-lollypop" "org-gnome-terminal" "org-gnome-nautilus" "spotify" "org-gnome-eolie" "discord" "org-gnome-screenshot" "audacity" "signal-desktop" "writer" "steam" "org-gnome-calendar" "org-gnome-contacts" "org-gnome-diskutility" "google-chrome" "wine-programs-t-mobile-digits" "us-zoom-zoom" "gnome-printers-panel" "org-gnome-gedit" "standard-notes" "org-gnome-fractal" "org-gnome-baobab" "org-gnome-evince" "org-gnome-clocks" "org-jitsi-jitsi-meet" "wine" "calc" "com-gitlab-newsflash" "org-gnome-podcasts" "gnome-system-monitor" "slack" "com-belmoussaoui-readitlater" "com-github-taiko2k-tauonmb" "brave-browser" "calibre-gui" "org-gnome-fileroller" "org-gnome-tweaks" "mindustry" "impress" "ripcord" "org-gnome-eog" "net-veloren-veloren" "vlc" "startcenter" "org-gnome-epiphany" "org-gnome-notes" "zoom" "com-uploadedlobster-peek" "org-gabmus-giara" "kodi" "calibre-ebook-viewer" "org-onlyoffice-desktopeditors" "de-haeckerfelix-shortwave" "org-gnome-calculator" "teams" "com-github-ismaelmartinez-teams-for-linux" "sm-puri-chatty" "yelp" "org-gnome-soundrecorder" "minecraft-launcher" "org-gnome-extensions" "ca-desrt-dconf-editor" "org-gnome-shell-extensions-gsconnect-preferences" "org-gnome-shell-extensions-gsconnect"];
      show-banners = true;
    };

    "org/gnome/desktop/peripherals/mouse" = {
      accel-profile = "default";
      left-handed = false;
      natural-scroll = false;
      speed = 1.0;
    };

    "org/gnome/desktop/peripherals/touchpad" = {
      click-method = "fingers";
      disable-while-typing = false;
      edge-scrolling-enabled = false;
      natural-scroll = true;
      send-events = "enabled";
      speed = 0.35;
      tap-to-click = true;
      two-finger-scrolling-enabled = true;
    };

    "org/gnome/desktop/search-providers" = {
      disabled = ["org.gnome.Eolie.desktop"];
      enabled = ["org.gnome.Weather.desktop"];
      sort-order = ["org.gnome.Contacts.desktop" "org.gnome.Documents.desktop" "org.gnome.Nautilus.desktop"];
    };

    "org/gnome/desktop/sound" = {
      allow-volume-above-100-percent = false;
      event-sounds = true;
      theme-name = "__custom";
    };

    "org/gnome/desktop/wm/keybindings" = {
      close = ["<Super>x"];
      minimize = ["<Super>BackSpace"];
      move-to-workspace-left = ["<Shift><Super>h"];
      move-to-workspace-right = ["<Shift><Super>l"];
      switch-applications = ["<Super><Tab>"];
      switch-applications-backward = [];
      switch-to-workspace-left = ["<Super>h"];
      switch-to-workspace-right = ["<Super>l"];
      switch-windows = ["<Super>Tab"];
      switch-windows-backward = ["<Shift><Super>Tab"];
      toggle-maximized = ["<Super>f"];
    };

    "org/gnome/shell/keybindings" = {
      focus-active-notification = [];
    };

    "org/gnome/desktop/wm/preferences" = {
      action-double-click-titlebar = "toggle-maximize";
      button-layout = "appmenu:close";
      workspace-names = ["Workspace 5" "Workspace 2" "Workspace 4" "Workspace 1" "Workspace 8" "Workspace 8" "Workspace 8" "Workspace 8" "Workspace 8"];
    };

    "org/gnome/epiphany" = {
      ask-for-default = false;
      default-search-engine = "DuckDuckGo";
      enable-webextensions = true;
    };

    "org/gnome/epiphany/state" = {
      download-dir = "/home/pachums";
      is-maximized = false;
      window-position = mkTuple [0 0];
      window-size = mkTuple [1890 1033];
    };

    "org/gnome/epiphany/web" = {
      default-zoom-level = 1.0;
      enable-mouse-gestures = true;
      switch-to-new-tab = true;
    };

    "org/gnome/evolution/calendar" = {
      time-divisions = 30;
      week-start-day-name = "monday";
      work-day-friday = true;
      work-day-monday = true;
      work-day-saturday = false;
      work-day-sunday = false;
      work-day-thursday = true;
      work-day-tuesday = true;
      work-day-wednesday = true;
    };

    "org/gnome/gnome-screenshot" = {
      border-effect = "none";
      delay = 0;
      include-border = true;
      include-pointer = true;
      last-save-directory = "file:///home/pachums/Pictures/Screenshots";
    };

    "org/gnome/gnome-system-monitor" = {
      network-total-in-bits = false;
      show-dependencies = false;
      show-whose-processes = "user";
    };

    "org/gnome/settings-daemon/plugins/color" = {
      night-light-enabled = true;
      night-light-schedule-automatic = true;
      night-light-schedule-to = 20.0;
      night-light-temperature = "uint32 2883";
    };

    "org/gnome/settings-daemon/plugins/media-keys" = {
      custom-keybindings = ["/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/"];
      screensaver = ["<Primary><Super>Delete"];
    };

    "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0" = {
      binding = "<Shift><Super>Return";
      command = "gnome-terminal";
      name = "Launch Terminal";
    };

    "org/gnome/settings-daemon/plugins/power" = {
      idle-dim = true;
      power-button-action = "suspend";
      sleep-inactive-ac-type = "nothing";
      sleep-inactive-battery-timeout = 1200;
      sleep-inactive-battery-type = "suspend";
    };

    "org/gnome/shell" = {
      app-picker-view = "uint32 1";
      disable-user-extensions = false;
      disabled-extensions = [
        "auto-move-windows@gnome-shell-extensions.gcampax.github.com"
        "apps-menu@gnome-shell-extensions.gcampax.github.com"
        "places-menu@gnome-shell-extensions.gcampax.github.com"
        "native-window-placement@gnome-shell-extensions.gcampax.github.com"
        "windowsNavigator@gnome-shell-extensions.gcampax.github.com"
        "CoverflowAltTab@dmo60.de"
        "window-list@gnome-shell-extensions.gcampax.github.com"
      ];
      enabled-extensions = [
        "GPaste@gnome-shell-extensions.gnome.org"
        "shellshape@gfxmonk.net"
        "launch-new-instance@gnome-shell-extensions.gcampax.github.com"
        "auto-move-windows@gnome-shell-extensions.gcampax.github.com"
        "drive-menu@gnome-shell-extensions.gcampax.github.com"
        "workspace-indicator@gnome-shell-extensions.gcampax.github.com"
        "appindicatorsupport@rgcjonas.gmail.com"
        "gsconnect@andyholmes.github.io"
        "vertical-overview@RensAlthuis.github.com"
      ];
      favorite-apps = ["firefox.desktop" "org.gnome.Console.desktop" "org.gnome.Nautilus.desktop" "com.github.flxzt.rnote.desktop"];
    };

    "org/gnome/shell/extensions/auto-move-windows" = {
      application-list = [];
    };

    "org/gnome/shell/extensions/gsconnect/messaging" = {
      window-maximized = false;
      window-size = mkTuple [640 480];
    };

    "org/gnome/shell/extensions/gsconnect/preferences" = {
      window-maximized = false;
      window-size = mkTuple [647 440];
    };

    "org/gnome/simple-scan" = {
      document-type = "photo";
      paper-height = 2794;
      paper-width = 2159;
      save-directory = "file:///home/pachums/Downloads/";
      save-format = "image/png";
    };

    "org/gnome/system/location" = {
      enabled = true;
    };

    "org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9" = {
      background-color = "rgb(23,20,33)";
      font = "Monospace 16";
      foreground-color = "rgb(208,207,204)";
      use-system-font = false;
      use-theme-colors = false;
    };

    "org/gnome/tweaks" = {
      show-extensions-notice = false;
    };

    "sm/puri/Chatty" = {
      blur-idle-buddies = true;
      experimental-features = true;
      first-start = false;
      greyout-offline-buddies = true;
      indicate-unknown-contacts = true;
      mam-enabled = true;
      message-carbons = false;
      return-sends-message = true;
      send-receipts = true;
      send-typing = false;
      window-maximized = false;
      window-size = mkTuple [1166 986];
    };

    "system/proxy" = {
      mode = "none";
    };
  };
}
