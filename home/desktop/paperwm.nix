{lib, ...}: let
  mkTuple = lib.hm.gvariant.mkTuple;
in {
  dconf.settings = {
    /*
    Let PaperWM handle keybindings
    "org/gnome/desktop/wm/keybindings" = {
      close = [ "<Super>x" ];
      minimize = [ "<Super>BackSpace" ];
      move-to-workspace-left = [ "<Shift><Super>h" ];
      move-to-workspace-right = [ "<Shift><Super>l" ];
      switch-applications = [ "<Alt><Tab>" ];
      switch-applications-backward = [];
      switch-to-workspace-left = [ "<Super>h" ];
      switch-to-workspace-right = [ "<Super>l" ];
      switch-windows = [ "<Super>Tab" ];
      switch-windows-backward = [ "<Shift><Super>Tab" ];
      toggle-maximized = [ "<Super>f" ];
    };
    */
    "/org/gnome/shell".enabled-extensions = [
      "paperwm@hedning:matrix.org"
    ];
    "org/gnome/shell/extensions/paperwm/keybindings" = {
      close-window = ["<Super>x"];
      move-down-workspace = ["<Shift><Super>m"];
      move-up-workspace = ["<Shift><Super>n"];
      new-window = ["<super>Return"];
      switch-down = ["<Super>j"];
      switch-down-workspace = ["<Super>m"];
      switch-left = ["<Super>h"];
      switch-right = ["<Super>l"];
      switch-up = ["<Super>k"];
      switch-up-workspace = ["<Super>n"];
      toggle-scratch = ["<Shift><Super>Escape"];
      toggle-scratch-layer = ["<Super>Escape"];
      toggle-scratch-window = [];
    };
    "org/gnome/shell/extensions/paperwm" = {
      disable-scratch-in-overview = true;
      override-hot-corner = false;
    };
    "org/gnome/mutter" = {
      edge-tiling = lib.mkForce false;
      attach-modal-dialogs = lib.mkForce false;
      workspaces-only-on-primary = lib.mkForce false;
    };
  };
}
